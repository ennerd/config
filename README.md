# The\Config

An interface to find configuration information. To access it, use \The\config('app.timezone'). To set config 
variables use The\config(['app.timezone' => 'Europe/Oslo']);

The default implementation uses php files located in the ./app/config folder. For example The\config('app.timezone')
will look for a file named ./app/config/app.php which should return an array: <?php return ['timezone' => 'Europe/Oslo'];.

Alternative location for the same configuration is ./app/config/app/timezone.php which must return a string:
<?php return 'Europe/Oslo';

## Usage

To access a configuration variable, simply do this:

```php
echo \The\config('some.variable');
```

## Interoperability

I accept pull requests if you want to provide interoperability with other frameworks. There are two ways a 
framework can be supported:

### Auto Detection

The src/the.php file has a function named "config". This function should detect if we are operating in
a framework such as Symfony, Laravel or Zend - and return the appropriate configuration.

### Composer Provides

The framework itself can provide the The\Config by declaring it in the composer.json file:

```json
{
    "provide": [ 'the/logger' ]
}
```

and then declaring a function in the The namespace like this

```php
namespace The {
    function logger() {
        /* return PSR-3 logger instance */
    }
}
```

