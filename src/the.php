<?php
namespace The;

/**
 * Get or set configuration data. This method works with any compliant config
 * backend.
 *
 * @param array|string $mixed
 * @return mixed
 */
function config($mixed) {
	if (is_string($mixed)) {
		return Config::get($mixed);
	} else if (is_array($mixed)) {
		return Config::set($mixed);
	} else {
		throw new \InvalidArgumentException('Expects string or array');
	}
}

\The::setDefaultClass( Config::class, Config\Config::class );
