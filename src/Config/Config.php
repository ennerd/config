<?php
namespace The\Config;

class Config implements ConfigInterface {

	protected static $config = [];

	public static function get($name) {
		if(isset($config[$name])) {
			return $config[$name];
		}

		/**
		 * Special cases. These can be initially set by calling The\Config::set([<key> => <value>, ...]);
		 */
		switch($name) {
			case 'app.paths.public' :
				$reflection = new \ReflectionClass(\Composer\Autoload\ClassLoader::class);
				return static::$config['app.public.path'] = dirname($reflection->getFileName(), 3);
			case 'app.paths.app' :
				return static::get('app.paths.public').'/app';
			case 'app.paths.public_files' :
				return static::get('app.paths.public').'/files';
			case 'app.paths.private_files' :
				return static::get('app.paths.base').'/private';
			case 'app.paths.base' :
				return $_SERVER['HOME'];
			case 'app.paths.config' :
				return static::get('app.paths.app').'/config';
		}

		// check on-disk configuration
		if (!is_dir($path = static::get('app.paths.config'))) {
			throw new ConfigMissingException("No configuration found on path '$path'.");
		}

		$parts = explode(".", $name);
		$numberOfParts = sizeof($parts);

		for($i = $numberOfParts-1; $i > 0; $i--) {
			var_dump(implode("/", array_slice($parts, 0, $i)));
		}
	}

	public static function set(array $config) {

	}
}
