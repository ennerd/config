<?php
namespace The\Config;

interface ConfigInterface {
	/**
	 * Find a configuration variable and return it. Should first check the the
	 * configuration variables that were set by ConfigInterface::set - before
	 * checking the persistent configuration.
	 *
	 * Given this order of execution:
	 *
	 * get('app')			returns ['path' => <path>, 'url' => <url>]
	 * get('app.path')		returns <path>
	 * set(['app' => 'frode'])	// legal, and does not overwrite app.path
	 * get('app.path')		returns <path> // even if app was overwritten
	 *
	 * @param string $name	Path/name of the configuration variable to return.
         * @return mixed
	 * @throws ConfigMissingException
	 */
	public static function get($name);

	/**
	 * Set configuration variables. Variables that have not been set this way
	 * must be loaded from disk.
         */
	public static function set(array $configPairs);
}
